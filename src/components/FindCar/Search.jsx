const Search = () => {
    return (
        <section className="cars-service">
            <div className="container cars-form bg-white rounded rounded-3">
                <form className="d-flex flex-wrap align-items-end justify-content-between">
                    <div className="p-2 form-item">
                        <label for="tipe" className="form-label">Tipe Driver</label>
                        <div className="input-group">
                            <input type="text" className="form-control" id="tipe" aria-describedby="emailHelp" />
                            <span className="input-group-text">
                                <img src="/icons/icon_arr_up.svg" alt="Panah" className="inp-arr" />
                            </span>

                            <div className="input-option bg-white" id="tipe_driver--option">
                                <p className="fs-6 p-2 mb-2 input-item">Dengan Sopir</p>
                                <p className="fs-6 p-2 mb-2  input-item">Tanpa Sopir</p>
                            </div>
                        </div>
                    </div>

                    <div className="p-2 form-item">
                        <label for="tanggal" className="form-label">Tanggal</label>
                        <div className="input-group">
                            <input type="date" className="form-control" id="tanggal" />
                            <span className="input-group-text">
                                <img src="/icons/icon_calender.png" alt="Icon Kalender" className="inp-cal" />
                            </span>
                        </div>
                    </div>

                    <div className="p-2 form-item">
                        <label for="waktu" className="form-label">Waktu Jemput/Ambil</label>
                        <div className="input-group">
                            <input type="text" className="form-control" id="waktu" autocomplete="off" />
                            <span className="input-group-text">
                                <img src="/icons/icon_clock.svg" alt="Icon Jam" className="clock" />
                            </span>

                            <div className="input-option bg-white waktu-form" id="tipe_waktu--option">
                                <p className="fs-6 p-2 mb-2 waktu-item">01:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">02:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">03:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">04:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">05:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">06:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">07:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">08:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">09:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">10:00 WIB</p>
                                <p className="fs-6 p-2 mb-2 waktu-item">15:00 WIB</p>
                            </div>
                        </div>
                    </div>

                    <div className="p-2 form-item">
                        <label for="jumlah" className="form-label">Jumlah Penumpang</label>
                        <div className="input-group">
                            <input type="number" className="form-control" id="jumlah" autocomplete="off" min="1" max="9" />
                            <span className="input-group-text">
                                <img src="/icons/icon_capacity.svg" alt="Icon Penumpang" class="inp-psg" />
                            </span>
                        </div>
                    </div>

                    <div className="p-2 form-item">
                        <button type="button" className="btn btn-submit" id="cari">Cari</button>
                    </div>
                </form>
            </div>

            <div className="container cars-avail pb-4 pt-4">
                <div className="car-container row gx-lg-5 px-0 gy-4">
                </div>
            </div>
        </section>
    )
}

export default Search;