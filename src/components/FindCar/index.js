import Navbar from "../LandingPage/Navbar"
import HeroDua from "./HeroDua";
import Search from "./Search";
import Layers from "./Layers";
import Footer from "../LandingPage/Footer"
import { useEffect, useState } from "react";
import axios from "axios";

const FindCar = () => {
    const [cars, setCars] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const renderCar = async () => {
        const carsData = await axios.get("http://localhost:3000/cars");
        setCars(carsData.data);
        setIsLoading(false);
    };

    const searchCar = async (option) => {
        const filterDate = new Date(`${option.date}T${option.time}`);
        console.log(filterDate);
        const carsData = await axios.get("http://localhost:3000/cars");
        const filterdCard = carsData.data.filter(
            (car) =>
                new Date(car.availableAt).getTime() === filterDate.getTime() &&
                car.capacity >= Number(option.capacity) &&
                car.available === true
        );
        setCars(filterdCard);
        setIsLoading(false);
    };

    useEffect(() => renderCar, []);


    return (
        <div className="find-car">
            <Navbar />
            <HeroDua />
            <Search searchCar={searchCar} />
            <Layers cars={cars} isLoading={isLoading} />
            <Footer />
        </div>
    )
}

export default FindCar;