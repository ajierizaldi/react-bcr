import DaftarMobil from "./DaftarMobil";

const CarBucket = ({ isLoading, cars }) => {
    return (
        <section className="cars-service">
            <div className="container cars-avail pb-4 pt-4">
                {isLoading && "Loading..."}
                {cars && <DaftarMobil cars={cars} />}
            </div>
        </section>
    );
};

export default CarBucket;