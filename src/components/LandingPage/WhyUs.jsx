const WhyUs = () => {
    return (
        <section className="why-us" id="why-us">
            <div className="container">
                <h3><b>Why Us?</b></h3>
                <p>Mengapa harus pilih Binar Car Rental?</p>

                {/* Card */}
                <div className="row">
                    <div className="col-sm-3 card-border">
                        <img src="images/icon-1.png" alt="Success" />
                        <h6>Mobil Lengkap</h6>
                        <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                    </div>
                    <div className="col-sm-3 card-border">
                        <img src="images/icon-2.png" alt="Price" />
                        <h6>Harga Murah</h6>
                        <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                    </div>
                    <div className="col-sm-3 card-border">
                        <img src="images/icon-3.png" alt="Time" />
                        <h6>Layanan 24 Jam</h6>
                        <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    </div>
                    <div className="col-sm-3 card-border">
                        <img src="images/icon-4.png" alt="Lamp" />
                        <h6>Sopir Professional</h6>
                        <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default WhyUs;