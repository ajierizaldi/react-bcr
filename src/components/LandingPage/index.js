import Navbar from "./Navbar"
import Hero from "./Hero"
import Service from "./Service"
import WhyUs from "./WhyUs"
import GettingStarted from "./GettingStarted"
import Faq from "./Faq"
import Footer from "./Footer"

const LandingPage = () => {
    return (
        <div className="landing-page">
            <Navbar />
            <Hero />
            <Service />
            <WhyUs />
            <GettingStarted />
            <Faq />
            <Footer />
        </div>
    )
}

export default LandingPage