const GettingStarted = () => {
    return (
        <section className="getting-started" id="getting-started">
            <div className="container">
                <div className="kolom-sewa text-center">
                    <div className="text">
                        <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <button className="btn btn-success">Mulai Sewa Mobil</button>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default GettingStarted;