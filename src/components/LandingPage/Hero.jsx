const Hero = () => {
    return (
        <section className="hero-section" id="hero-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 atas">
                        <h2><b>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</b></h2>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas <br /> terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu <br /> untuk sewa mobil selama 24 jam.
                        </p>
                        <button className="btn btn-success" onClick={() => window.location.href = "./FindCar"} > Cari Mobil</button>
                    </div>
                    <div className="col-md-6">
                        <img src="images/img_car.png" alt="Mobil" />
                    </div>
                </div>
            </div>
        </section >
    );
}

export default Hero;