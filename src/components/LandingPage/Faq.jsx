const Faq = () => {
    return (
        <section className="faq" id="faq">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <h3><b>Frequently Asked Questions</b></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                    <div className="col-md-6">
                        {/* Dropdown 1 */}
                        <button type="button" className="btn text-start" data-bs-toggle="collapse" data-bs-target="#satu">Apa saja syarat yang dibutuhkan?</button>
                        <div id="satu" className="collapse">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>

                        {/* Dropdown 2 */}
                        <button type="button" className="btn text-start" data-bs-toggle="collapse" data-bs-target="#dua">Berapa hari minimal sewa mobil lepas kunci?</button>
                        <div id="dua" clasName="collapse">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>

                        {/* Dropdown 3 */}
                        <button type="button" className="btn text-start" data-bs-toggle="collapse" data-bs-target="#tiga">Berapa hari sebelumnya sebaiknya booking sewa mobil?</button>
                        <div id="tiga" className="collapse">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>

                        {/* Dropdown 4 */}
                        <button type="button" className="btn text-start" data-bs-toggle="collapse" data-bs-target="#empat">Apakah ada biaya antar-jemput?</button>
                        <div id="empat" className="collapse">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>

                        {/* Dropdown 5 */}
                        <button type="button" className="btn text-start" data-bs-toggle="collapse" data-bs-target="#lima">Bagaimana jika terjadi kecelakaan?</button>
                        <div id="lima" className="collapse">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Faq;