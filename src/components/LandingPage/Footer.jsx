const Footer = () => {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolinggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                    <div className="col-md-2">
                        <ul>
                            <li><a href="#our-services"><p>Our services</p></a></li>
                            <li><a href="#why-us"><p>Why Us</p></a></li>
                            <li><a href="#testimonials"><p>Testimonial</p></a></li>
                            <li><a href="#faq"><p>FAQ</p></a></li>
                        </ul>
                    </div>
                    <div className="col-md-4">
                        <p>Connect with us</p>
                        <div className="row">
                            <div className="col img-list"><img src="images/fb-icon.png" alt="facebook" href="#" /></div>
                            <div className="col img-list"><img src="images/ig-icon.png" alt="instagram" href="#" /></div>
                            <div className="col img-list"><img src="images/twitter-icon.png" alt="twitter" href="#" /></div>
                            <div className="col img-list"><img src="images/mail-icon.png" alt="mail" href="#" /></div>
                            <div className="col img-list"><img src="images/twitch-icon.png" alt="twitch" href="#" /></div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <p>Copyright Binar 2022</p>
                        <img src="images/logo.png" alt="logo" className="footer-logo" />
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;