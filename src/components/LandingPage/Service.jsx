const Service = () => {
    return (
        <section className="service" id="services">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 mt-50">
                        <img src="images/img_service.png" alt="Service" />
                    </div>
                    <div className="col-md-6">
                        <h3><b>Best Car Rental for any kind of trip in (Lokasimu)!</b></h3>
                        <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                        <ul>
                            <li>Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                            <li>Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                            <li>Sewa Mobil Jangka Panjang Bulanan</li>
                            <li>Gratis Antar - Jemput Mobil di Bandara</li>
                            <li>Layanan Airport Transfer / Drop In Out</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Service;