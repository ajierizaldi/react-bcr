import { Routes, Route, } from 'react-router-dom'
import LandingPage from './components/LandingPage';
import FindCar from './components/FindCar';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/FindCar" element={<FindCar />} />
      </Routes>
    </div>
  );
}

export default App;
